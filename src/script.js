const hamb = document.querySelector('#hamb');
const popup = document.querySelector('#popup');
const menu = document.querySelector('#menu').cloneNode(1);
const body = document.body;

hamb.addEventListener('click', hambHandler);
function hambHandler(event) {
    event.preventDefault();
    popup.classList.toggle('open');
    hamb.classList.toggle('activeHamb');
    body.classList.toggle('noscroll');
    renderPopup();
}
function renderPopup() {
    popup.appendChild(menu);
}