const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const rename = require("gulp-rename");
const uglify = require('gulp-uglify-es').default;
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

gulp.task('default', function () {
	return gulp.src('dist', {read: false})
		.pipe(clean());
});

gulp.task( 'html', () => {
	return gulp.src( 'src/*.html')
		.pipe( gulp.dest( 'dist/' ) )
		.pipe( browserSync.stream() );
} );

gulp.task( 'styles', () => {
	return gulp.src( 'src/scss/**/*.scss' )
    .pipe(rename('styles.min.js'))
    .pipe(sass( { outputStyle: "expanded" } ).on( 'error', sass.logError ) )
		.pipe(autoprefixer() )
		.pipe( cleanCSS( { compatibility: 'ie8' } ) )
		.pipe( gulp.dest( 'dist/css' ) )
		.pipe( browserSync.stream() );
} );
gulp.task('clean:dist', function () {
    return gulp.src('dist', {read: false})
      .pipe(clean());
});
gulp.task('images', function() {
    return gulp.src('src/img/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/img'))
    .pipe( browserSync.stream() );
});
gulp.task( 'serve', () => {
	browserSync.init( {
		server: {
			baseDir: [ 'dist' ],
		},
		port: 3000,
		notify: false,
	} );
} )

gulp.task('uglify', function () {
	return gulp.src('src/script.js')
		.pipe(rename('scripts.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('dist/'));
});

gulp.task( 'build', gulp.series(
	'clean:dist',
	gulp.parallel( 'styles', 'uglify', 'html', 'images' )
) )
gulp.task( 'watch', function () {
	gulp.watch( 'src/scss/**/*.scss', gulp.series( 'styles' ) )
  gulp.watch( 'src/*.js', gulp.series( 'uglify' ) )
	gulp.watch( 'src/*.html', gulp.series( 'html' ) )
} )
gulp.task( 'dev',
	gulp.series( 'build', gulp.parallel( 'serve', 'watch' ) ) );